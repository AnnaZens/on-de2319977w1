import { Injectable, OnInit } from '@angular/core';
import { AdalService } from 'adal-angular4';
import { HttpClient } from '@angular/common/http';
import {Configuration} from '../services/logAnalytics/configuration'
import { environment } from '../../environments/environment';
import {QueryApi, Table} from '../services/logAnalytics/api'
import { Subject, Observable } from 'rxjs/RX';

@Injectable()
export class LoggingService implements OnInit{
  
  apiconfig: Configuration;
  user: any;
  queryApi : QueryApi;

  
  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }

  constructor(private adalService: AdalService, protected http: HttpClient) { 
    this.user = this.adalService.userInfo;
    this.apiconfig = {apiKey:'Bearer '+ this.user.token, basePath: environment.LOGANALYTICS_API_BASE_PATH, username:this.user.userName}
    const options = {configuration: this.apiconfig};
    this.queryApi = new QueryApi(this.apiconfig);
  }

  getFeedBack():Observable<Table[]>{
    const feedBack : Table[] = [];
    const subject = new Subject<Table[]>();
    this.queryApi.predefinedSearch("Feedback").then(function (response) {
      response["data"].forEach(element => {
        console.log("serve antwort")
        console.log(element)
        feedBack.push(element)
    });
      subject.next(feedBack);
    })
    return subject;

  }
}
