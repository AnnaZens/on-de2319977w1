import { Component, OnInit, ViewChild } from '@angular/core';
import { AdalService } from 'adal-angular4'
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import{LoggingService} from './logging.service'
import {QueryApi, Table} from '../services/logAnalytics/api'
import { MatPaginatorModule, MatPaginator } from '@angular/material/paginator'; 
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { process, State } from '@progress/kendo-data-query';

import {
    GridDataResult,
    DataStateChangeEvent,
    PageChangeEvent
} from '@progress/kendo-angular-grid';



export interface Logs {
  Datum: string;
  Konversationsnummer: string;
  Frage: string;
  Antwort: string;
  Userbewertung: string;
  Feedback: string;
}

   

 const tables: Logs[] = []

@Component({
  selector: 'app-logging',
  templateUrl: './logging.component.html',
  styleUrls: ['./logging.component.css']
})
export class LoggingComponent implements OnInit {

  displayedColumns: string[] = ['Datum', 'Frage', 'Antwort','Userbewertung','Feedback'];
  dataSource = new MatTableDataSource(tables);
  public columns: any[] = [{field: "Datum"}, {field: "Frage"}, {field: "Antwort"},{field: "Userbewertung"},{field: "Feedback"}];

  public view: Observable<GridDataResult>;
  public gridData : any = tables;
  public gridDataResult: GridDataResult;
  public pageSize = 20;
  public skip = 0;
  
  public dataLoading = true;

  feedBackTable : Logs[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;   

  @ViewChild(MatSort) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private adalService: AdalService, private logAnalyticsService: LoggingService) { }

  public state: State = {
    skip: 0,
    take: 20,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{ field: 'Datum', operator: 'contains', value: '' }]
    }
};

  ngOnInit() {
   
    this.logAnalyticsService.getFeedBack().subscribe(feedBackTables =>{ this.dataSource = new MatTableDataSource( this.transFormFeedBackTableToLog(feedBackTables[0]));
      //this.gridData = this.transFormFeedBackTableToLog(feedBackTables[0]);
      //this.table =  this.transFormFeedBackTableToLog(feedBackTables[0])
      this.dataLoading = false;
      this.feedBackTable = this.transFormFeedBackTableToLog(feedBackTables[0]);
      this.gridDataResult = {data: this.feedBackTable.slice(this.skip, this.skip + this.pageSize), total: this.feedBackTable.length};
      this.gridData = this.gridDataResult;
      this.dataLoading = false;
    
    })
    

  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.gridData = process(this.feedBackTable, this.state);
}

  public pageChange(event: PageChangeEvent): void {
    this.dataLoading = false;

    this.skip = event.skip;
    this.gridDataResult = {data:  this.feedBackTable.slice(this.skip, this.skip + this.pageSize), total:  this.feedBackTable.length};
    this.gridData = this.gridDataResult;

  }

  transFormFeedBackTableToLog(feedBack : Table):Logs[]{
    const logs : Logs[] = []  
    feedBack.rows.forEach(element => {
     
        const log : Logs = {Datum:this.converDate(element[0]),Konversationsnummer:element[1],Frage:element[4],Antwort:element[5],Userbewertung:this.convertUserBewertung(element[6]),Feedback : element[7]};
        logs.push(log)
      });
      return logs;
  }

  public converDate(date : string):string{
    return moment(date).format('DD.MM.YYYY HH:mm');
  }
  public convertUserBewertung (bewertung:string){
      if(bewertung == "True"){return "Positiv"}
      else{return "Negativ"}

  }
  public get authenticated(): boolean {
      return this.adalService.userInfo.authenticated;
    }

  

}
