import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse,HttpHeaders } from '@angular/common/http'

import { AdalService } from 'adal-angular4'
import {
  VideosApi,
  DtoSearchVideosResponseItem, 
  DtoLanguageResponse,
  DtoListVideosResponse,
  DtoSearchVideosResponse,
  DtoListVideosResponseItem,
  DtoSearchVideosRequest,
  DtoSearchVideosRequestInsightsEnum,
} from '../services/videoindexer/api'
import {VideoIndexerService } from '../videos/videoindexer.service'
import {ActivatedRoute} from '@angular/router';
import {Subject, Observable} from 'rxjs/RX';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../users/user.service';

@Component({
  selector: 'app-video-language-model',
  templateUrl: './video-language-model.component.html',
  styleUrls: ['./video-language-model.component.css']
})
export class VideoLanguageModelComponent implements OnInit {
  public progress: number;
  public message: string;

  userRole: string[]
  userName: string

  languagemodels: DtoLanguageResponse[]

  constructor(private adalService: AdalService,private toastr: ToastrService, private viService: VideoIndexerService, private router: ActivatedRoute, private http: HttpClient,private userService: UserService) { }

  ngOnInit() 
  {
    this.userName  = this.adalService.userInfo.userName
    this.userService.getRoleOfUser(this.userName).subscribe(result => {
      this.userRole = result})
    this.languagemodels = this.router.snapshot.data['languagemodels']
    this.viService.listLanguageModels().subscribe(lModels => {this.languagemodels = lModels})
    this.toastr.info("Sprachmodelle werden geladen")  
  
  }

  public get authenticated(): boolean {
  
    if(this.adalService.userInfo.authenticated && this.userRole.includes("Admin"))
    {
      return this.userService.isUserAdmin()
    }
    else{
      return false;
    }
  }

  downloadLanguageModelFile(modelId:string,fileId:string){
    this.viService.downloadLanguageModelFile(modelId,fileId).then(response => {this.downloadFile(response.data);this.toastr.success("Der Download wurde erfolgreich druchgeführt")})
  }

  downloadFile(data: string) {
    const blob = new Blob([data], { type: 'plain/text' });
    if (window.navigator && window.navigator.msSaveOrOpenBlob){
      window.navigator.msSaveOrOpenBlob(blob);  

    }
    else{
      const url= window.URL.createObjectURL(blob);
      window.open(url);
    }
   
  }

  upload(files) {
    console.log(files)
    if (files.length === 0)
      return;

    const formData = new FormData();

    for (let file of files){
      console.log(file.name, file)
      formData.append(file.name, file);}


    console.log("uploading")
    console.log(formData)
    this.viService.uploadModelWithFileName(formData);
  }

  uploadOld(files) {
    if (files.length === 0)
      return;

    const formData = new FormData();

    for (let file of files){
      console.log(file)
      formData.set(file.name, file);
      console.log(formData.get(file.name));
    }
       const headers = new Headers();
        headers.append('Content-Type', 'application/json');

      const uploadReq = new HttpRequest('POST', `https://dianaframeworkvideoindexerapi.azurewebsites.net/api/Files`, formData, {
      reportProgress: true,
      });
      
    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
      else if (event.type === HttpEventType.Response){
        this.message = event.body.toString();console.log(this.message)}
    });

    //upload file Name  to Model
  }
}
