import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { WebChatComponent } from './web-chat/web-chat.component';
import { ToastrModule } from 'ngx-toastr';


import { MaterialModule } from './material/material.module';
import {SafePipe} from './video-widget/safepipe'

import { AdalService, AdalGuard, AdalInterceptor } from 'adal-angular4';
import {MatInputModule,MatOptionModule, MatSelectModule, MatIconModule, MatCardModule} from '@angular/material'
import {FormsModule, ReactiveFormsModule} from '@angular/forms';




import {
  VideoListComponent,
  VideoThumbnailComponent,
  VideoListResolver,
  VideoIndexerService,
} from './videos'

// @ts-ignore
import {
  VideoWidgetRouteActivator,
  VideoWidgetComponent,
} from './video-widget'


import {
  UsersListComponent,
  UsersThumbnailComponent,
  UsersListResolver,
  UserService,
} from './users';
import { RoleAssignmentComponent } from './role-assignment/role-assignment.component';
import{ModulAssignmentDialog} from './module-assignment/module-assignment.component';
import { RoleTypeComponent } from './role-assignment/role-type.component';
import { DialogOverviewExampleDialog }  from './role-assignment/role-assignment.component';
import { MatDialogModule } from '@angular/material';
import {RoleService} from './role-assignment/role.service';
import { ModuleService } from './role-assignment/module.service';
import { VideoLanguageModelComponent } from './video-language-model/video-language-model.component' ;
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthorizationComponent } from './authorization/authorization.component';
import { ModuleAssignmentComponent } from './module-assignment/module-assignment.component';
import { UserManagementComponent, AssignUserDialog, DeleteUserDialog } from './user-management/user-management.component';
import { MediaDashboardComponent } from './media-dashboard/media-dashboard.component';
import { LoggingComponent } from './logging/logging.component';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import { LoggingService } from './logging/logging.service';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import { GridModule } from '@progress/kendo-angular-grid';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    ToolbarComponent,
    WebChatComponent,
    VideoListComponent,
    VideoThumbnailComponent,
    UsersListComponent,
    UsersThumbnailComponent,
    VideoWidgetComponent,
    SafePipe,
    RoleAssignmentComponent,
    RoleTypeComponent,
    DialogOverviewExampleDialog,
    AssignUserDialog,
    ModulAssignmentDialog,
    DeleteUserDialog,
    DashboardComponent,
    VideoLanguageModelComponent,
    AuthorizationComponent,
    ModuleAssignmentComponent,
    UserManagementComponent,
    MediaDashboardComponent,
    LoggingComponent,
  ],
  entryComponents: [DialogOverviewExampleDialog, ModulAssignmentDialog,AssignUserDialog,DeleteUserDialog,LoggingComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
	  ToastrModule.forRoot(),
    MaterialModule,
    HttpClientModule,
    MatDialogModule,
    MatInputModule,
    MatOptionModule, 
    MatSelectModule, 
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    FlexLayoutModule,
    MatSidenavModule,
    GridModule,
  ],
  providers: [
    VideoIndexerService,
    VideoListResolver,
    UserService,
    LoggingService,
    ModuleService,
    RoleService,
    UsersListResolver,
    VideoWidgetRouteActivator,
    AdalService,
    AdalGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AdalInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
