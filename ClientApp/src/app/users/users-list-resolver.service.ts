import {Injectable} from '@angular/core'
import {Resolve} from '@angular/router'
import {UserService} from '../users/user.service'
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class UsersListResolver implements Resolve<any> {
    constructor(private userService : UserService, private toastr: ToastrService){
    }

    resolve()
    {
        return this.userService.getusers().map(users => users)
    }

}