import {Injectable, EventEmitter} from '@angular/core'
import {Subject, Observable} from 'rxjs/RX'
import {UserFullData} from '../services/msgraphservice/lib/models'
import {Roles} from '../services/msgraphservice/lib/models'
import {UsersApi} from '../services/adminservice/api'
import {ApplicationUserApi} from '../services/adminservice/api'
import {DtoRolesResponse} from '../services/adminservice/api'
import {DtoUserResponse} from '../services/adminservice/api'
import globalAxios, { AxiosPromise, AxiosInstance } from 'axios';
import {Configuration} from '../services/adminservice/configuration'
import { Component, OnInit } from '@angular/core';
import { AdalService } from 'adal-angular4';
import { HttpClient } from '@angular/common/http';
import { forEach } from '@angular/router/src/utils/collection';
import { tmpdir } from 'os';
import { element } from '@angular/core/src/render3';
import { strict } from 'assert';
import {DtoApplicationUserRequest,DtoCatalogRequest,DtoCatalogResponseItem} from '../services/adminservice/api'
import {ModuleService} from '../role-assignment/module.service'
import { environment } from '../../environments/environment';

@Injectable()
export class UserService implements OnInit{

    user: any;
    usersApi : UsersApi;
    apiconfig: Configuration;
    appUserApi: ApplicationUserApi;
    modules:DtoCatalogResponseItem[]= []
    userWithModules: UserFullData [] = []
    userWithRoles: UserFullData [] = []


    constructor(private adalService: AdalService, protected http: HttpClient, private moduleService: ModuleService) {
        this.user = this.adalService.userInfo;
        this.apiconfig = {apiKey:'Bearer '+ this.user.token, basePath:environment.ADMINPORTAL_API_BASE_PATH, username:this.user.userName}
        const options = {configuration: this.apiconfig};
        this.usersApi = new UsersApi(this.apiconfig);
        this.appUserApi = new ApplicationUserApi(this.apiconfig);
        this.moduleService.getModules().subscribe(modules => this.modules = modules)
        this.getAllUserWithRoles().subscribe(rData => this.userWithRoles = rData)

      }
    
   public ngOnInit(): void {
        this.getAllUserWithModules().subscribe(uData => this.userWithModules = uData)
    }
    
    isUserAdmin():boolean{
       return  this.userWithRoles.filter(i => i.displayName === this.user.userName)[0].roleArray.includes("Admin")
    }
    
    splitAdUserName(userName:string): string{
        var tmp = userName.split("#")
        return tmp[0]
    }  

    getusers() :Observable<UserFullData[]>{
        let subject = new Subject<UserFullData[]>()
        const allUser = this.getAllAssignedUser();
        setTimeout(() => {subject.next(USERS); subject.complete();}, 100)
        return subject
    }

    getuser(id:string) : UserFullData{
        return USERS.find(user => user.id === id)
    }

    getAllUserWithRoles():Observable<UserFullData[]>{

        let self = this;
        const allUser : UserFullData[] = [];
        let subject = new Subject<UserFullData[]>()
        this.appUserApi.getAllUsersWithAssignedRolesAsync().then(function (response) {
            response["data"].forEach(element => {
                const tmpData = { id: element.id,
                displayName: self.splitAdUserName(element.name),
                mail: self.splitAdUserName(element.name),
                jobTitle: 'Developer',
                roleName:element.assignedRoles.map(role => role.name).toString(),
                roleArray:element.assignedRoles.map(role => role.name)
                }
                allUser.push(tmpData);
            });
          })
          setTimeout(() => {subject.next(allUser); subject.complete();}, 100)
          return subject;
    }

    getAllUserWithModules():Observable<UserFullData[]>{
        let self = this;
        const allUser : UserFullData[] = [];
        let subject = new Subject<UserFullData[]>()
        this.appUserApi.getAllUsersWithAssignedCatalogsAsync().then(function (response) {
            response["data"].forEach(element => {
                const tmpData = { id: element.id,
                displayName: self.splitAdUserName(element.name),
                mail: self.splitAdUserName(element.name),
                jobTitle: 'Developer',
                roleName:"",
                modules:element.assignedCatalogs.map(role => role.name).toString(),
                moduleArray : element.assignedCatalogs.map(role => role.name).sort(),
            }
                allUser.push(tmpData);
                             });
          })
          setTimeout(() => {subject.next(allUser); subject.complete();}, 100)
          return subject;
    }
    getAllAssignedUser():Observable<UserFullData[]>{
        let self = this;
        const allUser : UserFullData[] = [];
        let subject = new Subject<UserFullData[]>()
        this.usersApi.getAllAssignedUsers().then(function (response) {
            response["data"].forEach(element => {
                const tmpData = { id: element.id,
                displayName: self.splitAdUserName(element.name),
                mail: self.splitAdUserName(element.name),
                jobTitle: 'Developer',
                roleName:"",
                }
                self.getRoleOfUser(element.name).subscribe(role => {tmpData["roleName"] = role.toString();tmpData["roleArray"]=role;});  
                self.getModuelsForUser(element.name).subscribe(modules => {tmpData["modules"] = modules.toString();tmpData["moduleArray"]=modules.sort()});
                allUser.push(tmpData);
                             });
          })
          setTimeout(() => {subject.next(allUser); subject.complete();}, 100)
          return subject;
    }
    deleteUser(name:string):AxiosPromise<Boolean>{
        return this.appUserApi.deleteUser(name);
    }
    assignUser(name: string, roleName: string):AxiosPromise<Boolean>{
        var roleRequest:DtoApplicationUserRequest;
        roleRequest = {userName: name, role:roleName}
        return this.appUserApi.assignRole(roleRequest);
        
    }

    unAssignUser(name: string, roleName: string):AxiosPromise<Boolean>{
        var roleRequest:DtoApplicationUserRequest;
        roleRequest = {userName: name, role:roleName}
        return this.appUserApi.unAssignRole(roleRequest);
       
    }

    assignModule(name: string, moduleName: string):AxiosPromise<Boolean>{
        var catalogRequest:DtoCatalogRequest;
        catalogRequest = {userName: name, catalog:moduleName}
        return this.appUserApi.assignCatalog(catalogRequest);
        
    }

    unAssignModule(name: string, moduleName: string):AxiosPromise<Boolean>{
        var catalogRequest:DtoCatalogRequest;
        catalogRequest = {userName: name, catalog:moduleName}
        return this.appUserApi.unAssignCatalog(catalogRequest);
       
    }

    async changeUserRole(name:string, oldRole:string, newRole:string){
        var roleRequest:DtoApplicationUserRequest;
        roleRequest = {userName: name, role:oldRole}
        await this.appUserApi.unAssignRole(roleRequest).then(response => { 
            if (response["data"]==true) {
                roleRequest = {userName: name, role:newRole}
                this.appUserApi.assignRole(roleRequest).then(response => {console.log("end of assignment")});
        }
        }).then(response => {console.log("end of function")});

    }

    getuserByName(id:string) : UserFullData[]{
        const subject = new Subject<string>();
        let self = this;
        const allUser : UserFullData[] = [];
        this.usersApi.searchUsers(id).then(function (response) { response["data"].forEach(element => {
                const tmpData = {
                displayName: self.splitAdUserName(element),
                mail: self.splitAdUserName(element),
                jobTitle: 'Developer',
                roleName:""};
                allUser.push(tmpData);
            });
          })
        return allUser;
    }
 
    replaceAtSign(mail:string):string{
        if (mail.includes("@")){
            mail = mail.replace("@","_")
        }
        return mail
    }

    getRoleOfUser(mail:string):Observable<string[]>{
        const subject = new Subject<string[]>();
        this.appUserApi.getRolesForUserAsync(mail).then(function (response) {
            subject.next(response["data"])
            ;
          })
        return subject;  
    }

    //change Method
    getModulesOfUser(mail:string):Observable<string[]>{
     
        const subject = new Subject<string[]>();
        const assignedModules :string[] = []
        //call getAllUsersWithAssignedCatalogsAsync / get assigned catalogs fitler to name , map the assigned catalogs part to 
        this.modules.forEach(element => {
            this.appUserApi.isUserInCatalog({userName:mail,catalog:element["name"]}).then(function (response) {
                if(response["data"]){
                    assignedModules.push(element["name"])
                    subject.next(assignedModules);
                }
              })    
        });
        return subject;  
    }
   getModuelsForUser(mail:string):Observable<String[]>{
        let self = this;
        
        const assignedModules : string[] = [];
        let subject = new Subject<string[]>()

        this.appUserApi.getAllUsersWithAssignedCatalogsAsync().then(function (response) {
            response["data"].forEach(element => {
                if (mail == element.name){
                    element.assignedCatalogs.forEach(mod => {
                        assignedModules.push(mod["name"])
                    });
                    subject.next(assignedModules)
               }
             });
          })
          return subject;
    } 


    UserIsInModule(mail:string,module:string):Promise<boolean>{
        
        const subject = new Subject<boolean>();

       return this.appUserApi.isUserInCatalog({userName:mail,catalog:module}).then(function(response){
                return response.data
        })
        }
    }  




const USERS : UserFullData[] = [
    {
        id: "1",
        displayName: 'Ray Burks',
        mail: 'ray.burks@de.ey.com',
        jobTitle: 'Developer',
        roleId:"1",
        roleName:"Admin",
    },
    {
        id: "2",
        displayName: 'Mehmet Özkan',
        mail: 'mehmet.e.oezkan@de.ey.com',
        jobTitle: 'Developer',
        roleId:"1",
        roleName:"Admin",
    },
    {
        id: "3",
        displayName: 'Marcus Jainta',
        mail: 'marcus.jainta@de.ey.com',
        jobTitle: 'Projektleitung'
    },
    {
        id: "4",
        displayName: 'Ali Aksi',
        mail: 'ali.aksi@de.ey.com',
        jobTitle: 'Projektleitung'
    },
    {
        id: "5",
        displayName: 'Nico Hornung',
        mail: 'nico.hornung@daimler.com',
        jobTitle: 'Projektleitung'
    },
]