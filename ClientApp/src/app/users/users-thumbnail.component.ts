import { Component, Input } from '@angular/core'
import {UserFullData} from '../services/msgraphservice/lib/models'
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'user-thumbnail',
    templateUrl: './users-thumbnail.component.html',
    styles: [`
    .pad-left{margin-left:10px; }
    .thumbnail {min-height:80px;}
    .well div{color: #383434;}
    `
    ]
})

export class UsersThumbnailComponent {
    @Input() user:UserFullData
    
    constructor(private toastr: ToastrService) {

    }
}

