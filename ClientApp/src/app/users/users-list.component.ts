import { Component, OnInit } from '@angular/core'
import {ActivatedRoute} from '@angular/router'
import {UserService} from './user.service'
import { AdalService } from 'adal-angular4'
import {UserFullData} from '../services/msgraphservice/lib/models'
import { ToastrService } from 'ngx-toastr';

@Component({
   templateUrl: './users-list.component.html',
})

export class UsersListComponent implements OnInit {

  users:UserFullData[]  
  constructor(private adalService: AdalService, private toastr: ToastrService, private userService: UserService, private router: ActivatedRoute){

  }

  ngOnInit(){
    this.users = this.router.snapshot.data['users']
    this.userService.getusers().subscribe(users => {this.users = users})
  }


  public get authenticated(): boolean {
    return this.adalService.userInfo.authenticated;
  }
}