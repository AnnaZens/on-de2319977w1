import { Component, OnInit, Inject } from '@angular/core'
import {ActivatedRoute} from '@angular/router'
import {UserFullData} from '../services/msgraphservice/lib/models'
import {Roles} from '../services/msgraphservice/lib/models'
import {RoleService} from '../role-assignment/role.service'
import {UserService} from '../users/user.service'
import{ModuleService} from '../role-assignment/module.service'
import { AdalService } from 'adal-angular4'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DtoRolesResponse} from '../services/adminservice/api'
import {DtoCatalogResponseItem} from '../services/adminservice/api'
import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';

import {Input} from '@angular/core';
import { useAnimation } from '@angular/animations';
import { forkJoin } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { map } from 'rxjs-compat/operator/map';

export interface DialogData {
  user: string;
}

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  
  users:UserFullData[]
  usersWithModules:UserFullData[]
  userRole: string[]
  userName: string  
  constructor(private adalService: AdalService, private toastr: ToastrService, private userService: UserService,private roleService: RoleService,private moduleService:ModuleService, private router: ActivatedRoute,public dialog: MatDialog){}

  ngOnInit() {
    this.userName  = this.adalService.userInfo.userName
    this.users = this.router.snapshot.data['users']
    this.userService.getRoleOfUser(this.userName).subscribe(result => {
      this.userRole = result})
    this.userService.getAllAssignedUser().subscribe(users => this.users = users)

  }

  public get authenticated(): boolean {
  
    if(this.adalService.userInfo.authenticated && this.userRole.includes("Admin"))
    {
      return this.userService.isUserAdmin()
    }
    else{
      return false;
    }
  }

  mergeUserArray(userData: UserFullData[], moduleData:UserFullData[]) : UserFullData[]{
    console.log(userData)
    userData.forEach(element => {
      console.log("goes in")
      var element2 = moduleData.find(item => item.id == element["id"])
      console.log(element2)
      element.modules = element2.modules
    })
    ;
    return userData;

  }
  getUserFromAd(searchInput:string): void {
    if(searchInput.length >2){
    this.users = this.userService.getuserByName(searchInput); }
  }
  showAll():void{
    this.userService.getAllAssignedUser().subscribe(users => this.users = users)
  }

  openDialog(userName: string, roleName:string): void {
    const dialogRef = this.dialog.open(AssignUserDialog, {
      width: '450px',
      height: '200px',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //works        
      if (result["action"] == "assign"){ 
       
        this.userService.assignUser(userName,result["role"]["name"]).then(results => this.userService.getAllAssignedUser().subscribe(users => {this.users = users;
      }));
      this.toastr.success("Rolle wurde zugewiesen")   
      }
      else if (result["action"] == "unassign"){ 
        this.userService.unAssignUser(userName,result["role"]["name"]).then(results => this.userService.getAllAssignedUser().subscribe(users => {this.users = users; }))
        this.toastr.success("Rolle wurde abgemeldet")      }
      else{
      
      }
    
    });
    }
    openDeleteDialog(userName: string): void {
      const dialogRef = this.dialog.open(DeleteUserDialog, {
        width: '450px',
        height: '200px',
        disableClose: true,
        data: {user: userName}
      });
      dialogRef.afterClosed().subscribe(result => {
        //works        
        if (result["action"] == "delete"){ 
         
        this.userService.deleteUser(result["name"]).then(results => this.userService.getAllAssignedUser().subscribe(users => {this.users = users;
        }));
        this.toastr.success("Der User wurde entfernt")   
        }
        else{
        }
      
      });
      }
 
} 

@Component({
  selector: 'assignUserDialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class AssignUserDialog {
  roles:DtoRolesResponse[]= []
  @Input() value:string;

  constructor(
    public dialogRef: MatDialogRef<AssignUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private roleService: RoleService, private router: ActivatedRoute ) {}

  onAssignClick(): void {
    this.dialogRef.close({role:this.value, action:"assign"});
  }
  onUnAssignClick(): void {
    this.dialogRef.close({role:this.value, action:"unassign"});
  }
  onCancelClick(): void {
    this.dialogRef.close({role:this.value, action:"cancel"});
  }

  ngOnInit() {
    this.roles = this.router.snapshot.data['roles']
    this.roleService.getRoles().subscribe(roles =>{this.roles = roles})
  }

}

@Component({
  selector: 'deleteUserDialog',
  templateUrl: 'deleteUser-dialog.html',
})
export class DeleteUserDialog {
  roles:DtoRolesResponse[]= []
  @Input() value:string;

  constructor(
    public dialogRef: MatDialogRef<DeleteUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private roleService: RoleService, private router: ActivatedRoute ) {}

    onDeleteClick(): void {
    this.dialogRef.close({name:this.value, action:"delete"});
  }

  onCancelClick(): void {
    this.dialogRef.close({roleName:this.value, action:"cancel"});
  }

  ngOnInit() {
    this.value = this.data.user;
    console.log(this.value)
  }

}

