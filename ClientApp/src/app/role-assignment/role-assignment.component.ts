import { Component, OnInit, Inject } from '@angular/core'
import {ActivatedRoute} from '@angular/router'
import {UserFullData} from '../services/msgraphservice/lib/models'
import {Roles} from '../services/msgraphservice/lib/models'
import {RoleService} from './role.service'
import {UserService} from '../users/user.service'
import{ModuleService} from './module.service'
import { AdalService } from 'adal-angular4'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DtoRolesResponse} from '../services/adminservice/api'
import {DtoCatalogResponseItem} from '../services/adminservice/api'
import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';

import {Input} from '@angular/core';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-role-assignment',
  templateUrl: './role-assignment.component.html',
  styleUrls: ['./role-assignment.component.css']
})
export class RoleAssignmentComponent implements OnInit {
  users:UserFullData[]
  userRole: string[]
  userName: string  
  constructor(private adalService: AdalService, private toastr: ToastrService, private userService: UserService,private roleService: RoleService,private moduleService:ModuleService, private router: ActivatedRoute,public dialog: MatDialog){}

  ngOnInit() {
    this.userName  = this.adalService.userInfo.userName

    this.users = this.router.snapshot.data['users']
    //this.userService.getAllAssignedUser().subscribe(users => this.users = users)
   // this.userService.getAllUserWithRoles().subscribe(users => this.users = users)
   //this.userService.deleteUser("ali.aksi_de.ey.com") works
   this.userService.getRoleOfUser(this.userName).subscribe(result => {
    this.userRole = result})
   this.userService.getAllUserWithRoles().subscribe(users => this.users = users)

  }

  public get authenticated(): boolean {
  
    if(this.adalService.userInfo.authenticated && this.userRole.includes("Admin"))
    {
      return this.userService.isUserAdmin()
    }
    else{
      return false;
    }
  }

  getUserFromAd(searchInput:string): void {
    if(searchInput.length >4){
    this.users = this.userService.getuserByName(searchInput); }
  }
  showAll():void{
    this.userService.getAllAssignedUser().subscribe(users => this.users = users)
  }

  openDialog(userName: string, roleName:string): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '550px',
      height: '200px',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //works        
      if (result["action"] == "assign"){ 
       
        this.userService.assignUser(userName,result["role"]["name"]).then(results => this.userService.getAllAssignedUser().subscribe(users => {this.users = users;
      }));
      this.toastr.success("Rolle wurde zugewiesen")   
      }
      else if (result["action"] == "unassign"){ 
        this.userService.unAssignUser(userName,result["role"]["name"]).then(results => this.userService.getAllAssignedUser().subscribe(users => {this.users = users; }))
        this.toastr.success("Rolle wurde abgemeldet")      }
      else{
      
      }
    });
    }
 
} 

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {
  roles:DtoRolesResponse[]= []
  @Input() value:string;

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private roleService: RoleService, private router: ActivatedRoute ) {}

  onAssignClick(): void {
    this.dialogRef.close({role:this.value, action:"assign"});
  }
  onUnAssignClick(): void {
    this.dialogRef.close({role:this.value, action:"unassign"});
  }
  onCancelClick(): void {
    this.dialogRef.close({role:this.value, action:"cancel"});
  }

  ngOnInit() {
    this.roles = this.router.snapshot.data['roles']
    this.roleService.getRoles().subscribe(roles =>{this.roles = roles})
  }

}
