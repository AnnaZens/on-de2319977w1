import {Injectable, EventEmitter} from '@angular/core'
import {Subject, Observable} from 'rxjs/RX'
import {Roles, UserFullData} from '../services/msgraphservice/lib/models'
import {RolesApi} from '../services/adminservice/api'
import {DtoRolesResponse} from '../services/adminservice/api'
import globalAxios, { AxiosPromise, AxiosInstance } from 'axios';
import {Configuration} from '../services/adminservice/configuration'
import { Component, OnInit } from '@angular/core';
import { AdalService } from 'adal-angular4';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class RoleService implements OnInit{
    user: any;
    rolesApi : RolesApi;
    apiconfig: Configuration;
    userWithRoles: UserFullData [] = []

    constructor(private adalService: AdalService, protected http: HttpClient) {
        this.user = this.adalService.userInfo;
        this.apiconfig = {apiKey:'Bearer '+ this.user.token, basePath: environment.ADMINPORTAL_API_BASE_PATH, username:this.user.userName}
        const options = {configuration: this.apiconfig};
        this.rolesApi = new RolesApi(this.apiconfig);
        //this.viIntegrationService = new VideoIndexIntegration(options);
    }
    ngOnInit(): void {

        throw new Error("Method not implemented.");
    }

    getroles() : Observable<DtoRolesResponse[]>{
      let subject = new Subject<DtoRolesResponse[]>()
      setTimeout(() => {subject.next(ROLES); subject.complete();}, 100)
      return subject
    }

    getrole(name:string) : Roles{
        return ROLES.find(role => role.name === name)
    }
    
    getRoles():Observable<DtoRolesResponse[]>{
        const roles : DtoRolesResponse[] = [];
        const subject = new Subject<DtoRolesResponse[]>();
        this.rolesApi.getAllRolesAsync().then(function (response) {
            response["data"].forEach(element => {
                roles.push(element)
            });
            subject.next(roles);
          })
        return subject;
    }

}

const ROLES : Roles[] = [
    {
        id: "1",
        name: 'Admin',
    },
    {
        id: "2",
        name:"Customer",
    }
]