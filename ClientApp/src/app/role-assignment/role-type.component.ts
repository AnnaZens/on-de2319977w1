import { Component, Input } from '@angular/core';
import {UserFullData} from '../services/msgraphservice/lib/models'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-role-type',
  templateUrl: './role-type.component.html',
  styleUrls: ['./role-type.component.css']
})


export class RoleTypeComponent {
  @Input() user:UserFullData
  constructor(private toastr: ToastrService) { }


}
