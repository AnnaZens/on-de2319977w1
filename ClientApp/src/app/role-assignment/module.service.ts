import {Injectable, EventEmitter} from '@angular/core'
import {Subject, Observable} from 'rxjs/RX'
import {Roles} from '../services/msgraphservice/lib/models'
import {RolesApi, CatalogApi} from '../services/adminservice/api'
import {DtoRolesResponse} from '../services/adminservice/api'
import globalAxios, { AxiosPromise, AxiosInstance } from 'axios';
import {Configuration} from '../services/adminservice/configuration'
import { Component, OnInit } from '@angular/core';
import { AdalService } from 'adal-angular4';
import { HttpClient } from '@angular/common/http';
import {DtoCatalogResponseItem} from '../services/adminservice/api'
import { environment } from '../../environments/environment';

@Injectable()
export class ModuleService{
    user: any;
    modulesApi : CatalogApi;
    apiconfig: Configuration;

    constructor(private adalService: AdalService, protected http: HttpClient) {
        this.user = this.adalService.userInfo;
        this.apiconfig = {apiKey:'Bearer '+ this.user.token, basePath:environment.ADMINPORTAL_API_BASE_PATH, username:this.user.userName}
        const options = {configuration: this.apiconfig};
        this.modulesApi = new CatalogApi(this.apiconfig);
        //this.viIntegrationService = new VideoIndexIntegration(options);
      }
    
    getModules():Observable<DtoCatalogResponseItem[]>{
        const roles : DtoCatalogResponseItem[] = [];
        const subject = new Subject<DtoCatalogResponseItem[]>();
        this.modulesApi.getAllCatalogs().then(function (response) {
            response["data"].forEach(element => {
                roles.push(element)
            });
            subject.next(roles);
          })
        return subject;
    }


}

