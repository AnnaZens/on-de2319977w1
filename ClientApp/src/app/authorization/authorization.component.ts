import { Component, OnInit } from '@angular/core';
import {Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr';
import {UserService} from '../users/user.service'
import { AdalService } from 'adal-angular4';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.css']
})
export class AuthorizationComponent implements OnInit {

  constructor(private router: Router, private toastr: ToastrService, private userService:UserService,private adalService: AdalService) { }

  ngOnInit() {
  }

  openRoleAssignment() {
    this.router.navigate(['Nutzerverwaltung/Rollen']);
  }

  openModuleAssignment() {
    this.router.navigate(['Nutzerverwaltung/Module']);
  }

  openUserManagement() {
    this.router.navigate(['Nutzerverwaltung/Benutzer']);
  }
}
