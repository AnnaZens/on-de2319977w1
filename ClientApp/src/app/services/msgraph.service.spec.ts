import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MSGraphIntegrationApiV1 } from './msgraphservice/lib/mSGraphIntegrationApiV1';

describe('Service: MSGraphService', () => {
  let graphService: MSGraphIntegrationApiV1;

  beforeEach(() => { 
    let options = {baseUri: 'http://localhost:5000'};
    graphService = new MSGraphIntegrationApiV1(options);    
  });

  // it('get users', async (done) {
  //   expect(graphService).toBeDefined();
  //   let users = await graphService.getUsers();
  //   expect(users).toBeDefined();
  // });

  it('get users', async (done) => {
    try {
      expect(graphService).toBeDefined();
      let users = await graphService.getUsers();
      expect(users).toBeDefined();
      done();
    } catch (err) {
      done.fail(err);
    }
  });

  it('test async', async (done) => {
    try {
      done();
    } catch (err) {
      done.fail(err);
    }
  });


  afterEach(() => {
    graphService = null;
  });
});
