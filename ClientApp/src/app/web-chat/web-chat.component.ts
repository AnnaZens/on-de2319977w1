import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { AdalService } from 'adal-angular4';
import {Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr';

/**
 * Declares the WebChat property on the window object.
 */
declare global {
    interface Window {
        WebChat: any;
    }
}

window.WebChat = window.WebChat || {};

@Component({
    selector: "web-chat-app",
    templateUrl: "./web-chat.component.html",
    styleUrls: ["./web-chat.component.css"]
})
export class WebChatComponent implements OnInit {
    @ViewChild("botWindow") botWindowElement: ElementRef;

    constructor(private adalService: AdalService, private toastr: ToastrService, private router: Router) { }

    //public ngOnInit(): void {
    public async ngOnInit() {
        this.adalService.handleWindowCallback();

        // const directLine = window.WebChat.createDirectLine({
        //     secret: "vhvOxnz6Jhw.nWYJBwW2nynLDV6ngUm4X23pSxOeK0OIt33Cj9MFIV0",
        //     webSocket: false
        // });

        const directLine = window.WebChat.createDirectLine({
            secret: "1X4jPsy0XW8.bD_lElC9klMjKGLGV8RB8FGBc2-8uGYlwDfj3zJMOKc",
            webSocket: false
        });

        // const searchParams = new URLSearchParams(window.location.search);
        // const subscriptionKey = searchParams.get('s');        
        // const webSpeechPonyfillFactory = await window.WebChat.createCognitiveServicesSpeechServicesPonyfillFactory({
        //     region: searchParams.get('r') || 'westus',
        //     subscriptionKey
        // });

        var selectedLanguage = "de-DE";
        var selectedVoice = "Microsoft Server Speech Text to Speech Voice (de-DE, KatjaNeural)";
    
        const res = await fetch('https://westeurope.api.cognitive.microsoft.com/sts/v1.0/issuetoken', {
        headers: {
            'Ocp-Apim-Subscription-Key': '02e85dc721054e1e9db5b4fedadc3dba'
        },
        method: 'POST'
        });
        const authorizationToken = await res.text();
        const region = "westeurope";

        async function createHybridPonyfillFactory({ authorizationToken, region }) {
        const webSpeechPonyfillFactory = await window.WebChat.createCognitiveServicesSpeechServicesPonyfillFactory({ authorizationToken, region });
            return options => {
                const ponyfill = webSpeechPonyfillFactory(options);
                var speechSynthesisUtterance = ponyfill.SpeechSynthesisUtterance;
                var speechSynthesis = ponyfill.speechSynthesis;

                speechSynthesis.getVoices = function () {
                    return [{ lang: selectedLanguage, gender: 'Female', voiceURI: selectedVoice }];
                }

                return {
                    SpeechGrammarList: ponyfill.SpeechGrammarList,
                    SpeechRecognition: ponyfill.SpeechRecognition,
                    speechSynthesis: speechSynthesis,
                    SpeechSynthesisUtterance: speechSynthesisUtterance
                }
            };
        }

        window.WebChat.renderWebChat(
            {
                directLine: directLine,
                webSpeechPonyfillFactory: await createHybridPonyfillFactory({ authorizationToken, region }),
                userID: "DianaId",
            },
            this.botWindowElement.nativeElement
        );

        directLine
            .postActivity({
                from: { id: "DianaId", name: "DianaUser" },
                name: "requestWelcomeDialog",
                type: "event",
                value: "token"
            })
            .subscribe(
                id => console.log(`Posted activity, assigned ID ${id}`),
                error => console.log(`Error posting activity ${error}`)
            );
    }

    public get authenticated(): boolean {
        return this.adalService.userInfo.authenticated;
    }
}
