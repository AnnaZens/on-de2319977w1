import { Component, OnInit, Inject } from '@angular/core'
import {ActivatedRoute} from '@angular/router'
import {UserFullData} from '../services/msgraphservice/lib/models'
import {Roles} from '../services/msgraphservice/lib/models'
import {RoleService} from '../role-assignment/role.service'
import {UserService} from '../users/user.service'
import{ModuleService} from '../role-assignment/module.service'
import { AdalService } from 'adal-angular4'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DtoRolesResponse} from '../services/adminservice/api'
import {DtoCatalogResponseItem} from '../services/adminservice/api'
import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';

import {Input} from '@angular/core';

export interface DialogData {
  user: string;
  roleNames: UserFullData [];
}
@Component({
  selector: 'app-module-assignment',
  templateUrl: './module-assignment.component.html',
  styleUrls: ['./module-assignment.component.css']
})
export class ModuleAssignmentComponent implements OnInit {
  
  users:UserFullData[]
  userWithRoles: UserFullData [] = []
  userRole: string[]
  userName: string

  constructor(private adalService: AdalService, private toastr: ToastrService, private userService: UserService,private roleService: RoleService,private moduleService:ModuleService, private router: ActivatedRoute,public dialog: MatDialog){}

  ngOnInit() {
    this.userName  = this.adalService.userInfo.userName
    this.users = this.router.snapshot.data['users']
    //this.userService.getAllAssignedUser().subscribe(users => this.users = users)
   // this.userService.getAllUserWithRoles().subscribe(users => this.users = users)
   //this.userService.deleteUser("ali.aksi_de.ey.com") works
   this.userService.getAllUserWithModules().subscribe(users => this.users = users)
   this.userService.getAllUserWithRoles().subscribe(userRoles => this.userWithRoles = userRoles)
    this.userService.getRoleOfUser(this.userName).subscribe(result => {
      this.userRole = result})
  }

  public get authenticated(): boolean {
  
    if(this.adalService.userInfo.authenticated && this.userRole.includes("Admin"))
    {
      return this.userService.isUserAdmin()
    }
    else{
      return false;
    }
  }

  getUserFromAd(searchInput:string): void {
    if(searchInput.length >4){
    this.users = this.userService.getuserByName(searchInput); }
  }
  showAll():void{
    this.userService.getAllAssignedUser().subscribe(users => this.users = users)
  }

    openModuleDialog(userName: string, roleName:string[]): void {
      const dialogRef = this.dialog.open(ModulAssignmentDialog, {
        width: '550px',
        height: '200px',
        disableClose: true,
        data: {user: userName, roleNames:this.userWithRoles}
      });
      dialogRef.afterClosed().subscribe(result => {
        //works        
        if (result["action"] == "assign"){ 
         this.userService.assignModule(userName,result["module"]["name"]).then(results => this.userService.getAllUserWithModules().subscribe(users => {this.users = users;}))
         this.toastr.success("Das Modul wurde zugewiesen")
        }
        else if (result["action"] == "unassign"){ 
          this.userService.unAssignModule(userName,result["module"]["name"]).then(results => this.userService.getAllUserWithModules().subscribe(users => {this.users = users}))
          this.toastr.success("Das Modul wurde abgemeldet")
        }
        else{
        
        }
      
      });
      }
} 



@Component({
  selector: 'module-dialog',
  templateUrl: 'module-dialog.html',
})
export class ModulAssignmentDialog {
  modules:DtoCatalogResponseItem[]= []
  @Input() value:string;
  constructor(
    public dialogRef: MatDialogRef<ModulAssignmentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private roleService: RoleService, private moduleService:ModuleService, private router: ActivatedRoute ) {}

  onAssignClick(): void {
    this.dialogRef.close({module:this.value, action:"assign"});
  }
  onUnAssignClick(): void {
    this.dialogRef.close({module:this.value, action:"unassign"});
  }
  onCancelClick(): void {
    this.dialogRef.close({module:this.value, action:"cancel"});
  }
  ngOnInit() {
    this.modules = this.router.snapshot.data['modules']
    var userWithRole = this.data.roleNames.find(i => i.displayName === this.data.user)
    if(userWithRole.roleArray.includes("Admin")){
    this.moduleService.getModules().subscribe(modules =>{this.modules = modules})}
    else if(userWithRole.roleArray.includes("Customer")){
      this.moduleService.getModules().subscribe(modules =>{this.modules = modules.filter(m => m.name === "WebChatModule");})

    }
  
  }

}
