import { Component, OnInit } from '@angular/core';
import {Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr';
import {UserService} from '../users/user.service'
import { AdalService } from 'adal-angular4';

@Component({
  selector: 'app-media-dashboard',
  templateUrl: './media-dashboard.component.html',
  styleUrls: ['./media-dashboard.component.css']
})
export class MediaDashboardComponent implements OnInit {

  constructor(private router: Router, private toastr: ToastrService, private userService:UserService,private adalService: AdalService) { }

  ngOnInit() {
  }

  languageModelVideos() {
    this.router.navigate(['Medien/sprachmodelle']);
  }  

  listVideos() {
    this.router.navigate(['Medien/videos']);
  }
}
