import { Component, OnInit } from '@angular/core'
import {ActivatedRoute} from '@angular/router'
import {VideoIndexerService} from '../videos/videoindexer.service'
import {DtoListVideosResponseItem} from '../services/videoindexer/api'
import { AdalService } from 'adal-angular4';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../users/user.service';
import * as moment from 'moment';

@Component({
    selector: "video-list-app",
    templateUrl: "./video-list.component.html",
    styleUrls: ["./video-list.component.css"]
})

export class VideoListComponent implements OnInit {

  videos:DtoListVideosResponseItem[];
  userRole: string[]
  userName: string

  constructor(private adalService: AdalService, private toastr: ToastrService, private viService: VideoIndexerService, private router: ActivatedRoute,private userService: UserService){

  }

  ngOnInit(){
    this.userName  = this.adalService.userInfo.userName
    this.userService.getRoleOfUser(this.userName).subscribe(result => {
      
      this.userRole = result})
 
      this.viService.listVideosAsync().then(response => {
       
      this.videos = response.mediaItems;
      this.toastr.info("Liste der verfügbaren Videos")
    });
  }
  


  public get authenticated(): boolean {
  
    if(this.adalService.userInfo.authenticated && this.userRole.includes("Admin"))
    {
      return this.userService.isUserAdmin()
    }
    else{
      return false;
    }
  }
}
