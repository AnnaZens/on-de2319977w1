import {Injectable, EventEmitter} from '@angular/core';
import {Subject, Observable} from 'rxjs/RX';

import {
    VideosApi,
    FilesApi,
    DtoLanguageResponse,
    DtoSearchVideosResponseItem, 
    DtoListVideosResponse,
    DtoSearchVideosResponse,
    DtoListVideosResponseItem,
    DtoVideoWidgetResponse,
    DtoVideoIndexResponse,
    DtoSearchVideosRequest,
    DtoSearchVideosRequestInsightsEnum,

} from '../services/videoindexer/api'

import {Configuration} from '../services/videoindexer/configuration'
import { AdalService } from 'adal-angular4';
import {VideoWidget} from '../video-widget/video-widget'
import globalAxios, { AxiosPromise, AxiosInstance } from 'axios';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class VideoIndexerService {
    viIntegrationService: VideosApi;
    apiconfig: Configuration;
    filesService:FilesApi

    constructor(private adalService: AdalService, protected http: HttpClient) {
        const user = this.adalService.userInfo;
        this.apiconfig = new Configuration();
        this.apiconfig.basePath = 'https://dianaframeworkvideoindexerapi.azurewebsites.net';
        this.apiconfig.apiKey='Bearer '+ user.token
        this.apiconfig.username = user.userName
        this.viIntegrationService = new VideosApi(this.apiconfig);
        this.filesService = new FilesApi(this.apiconfig);
    }
    
    listVideos():Observable<DtoListVideosResponse>{
        let self = this;
        let dtoResponse : DtoListVideosResponse = {mediaItems: new Array<DtoListVideosResponseItem>() };
        let subject = new Subject<DtoListVideosResponse>()

        this.viIntegrationService.listVideos().then(response => {
            dtoResponse = response.data;
        })
        setTimeout(() => {
            subject.next(dtoResponse); 
            subject.complete();
        }, 1000)
        return subject;
    }


    listLanguageModels():Observable<DtoLanguageResponse[]>{
        let self = this;
        let dtoResponse : Array<DtoLanguageResponse>;
        let subject = new Subject<Array<DtoLanguageResponse>>()

        this.viIntegrationService.getLanguageModels().then(response => {
            dtoResponse = response.data;
        })
        setTimeout(() => {
            subject.next(dtoResponse); 
            subject.complete();
        }, 1000)

        return subject;
    }

    uploadModelWithFileName(media:any)
    {
    this.filesService.uploadFile(media).then(response => console.log(response))
    }

    downloadLanguageModelFile(modelId:string,fileId:string):AxiosPromise<string>{
    return this.viIntegrationService.downloadLanguageModelFile(modelId,fileId);
    }

    listVideosAsync():Promise<DtoListVideosResponse>{
        let promise = new Promise((resolve, reject) => {
            this.viIntegrationService.listVideos().then(response => {
                resolve(response.data);
            });
          });
          return promise;
    }



    getVideoWidget(id: string):Observable<VideoWidget>{
        let self = this;
        let dtoResponse : DtoVideoWidgetResponse = {};
        let widget : VideoWidget = {id:id};
        let subject = new Subject<VideoWidget>()

        this.viIntegrationService.getVideoWidgets(id).then(response => {
            dtoResponse = response.data;
            widget.widgetinsights = dtoResponse.embeddedInsightsUrl;
            widget.widgetplayer = dtoResponse.embeddedPlayerUrl;
        })
        setTimeout(() => {
            subject.next(widget); 
            subject.complete();
        }, 2000)
        return subject;
    }

   getVideoWidgetAsync(id: string):Promise<VideoWidget>{
        let promise = new Promise<VideoWidget>((resolve, reject) => {
            this.viIntegrationService.getVideoWidgets(id).then(response => {
                let widget : VideoWidget = {id:id};
                widget.widgetinsights = response.data.embeddedInsightsUrl;
                widget.widgetplayer = response.data.embeddedPlayerUrl;
                resolve(widget);
            })    
        });
        return promise;
    } 

}

