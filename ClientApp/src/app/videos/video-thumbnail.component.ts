import { Component, Input } from '@angular/core'
import { DtoListVideosResponseItem } from '../services/videoindexer/api'
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'video-thumbnail',
    template: ` 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    
    <div [routerLink]="['/videos', video.id]" class ="well hoverwell thumbnail" style="margin-left:15px">
        <h2>{{video?.name | uppercase}}</h2>
        <div *ngIf="video?.userName">
        Username: {{video?.userName}}
        </div>
        <div *ngIf="video?.durationInSeconds">
        <label>Dauer in Sekunden: {{video?.durationInSeconds}}</label>
    </div>
    </div>`,
    styles: [`
    .pad-left{margin-left:10px; }
    .thumbnail {min-height:210px;}
    .well div{color: #bbb;}
    `
    ]
})

export class VideoThumbnailComponent {
    @Input() video:DtoListVideosResponseItem
    
    constructor(private toastr: ToastrService) {

    }
}

