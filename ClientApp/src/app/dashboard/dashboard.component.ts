import { Component, OnInit } from '@angular/core';
import {Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr';
import {UserService} from '../users/user.service'
import { AdalService } from 'adal-angular4';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  assignedModules: string[]
  userName: string
  webAuthenticated: boolean
  videoIndexerAuthenticated: boolean
  userManagementAuthenticated: boolean
  userRole: string[]

  constructor(private router: Router, private toastr: ToastrService, private userService:UserService,private adalService: AdalService) { }

  ngOnInit() {
    this.userName  = this.adalService.userInfo.userName
    console.log(this.userName)
    //this.userService.getModulesOfUser(this.userName).subscribe(results =>{console.log(results); this.assignedModules = results})
    this.userService.getRoleOfUser(this.userName).subscribe(result => {
      this.userRole = result
        
      if(this.adalService.userInfo.authenticated){

        if(this.userRole.includes("Customer"))  
        this.userService.UserIsInModule(this.userName,"WebChatModule").then(result => this.webAuthenticated = result)
        
        if(this.userRole.includes("Admin")){  
        this.userService.UserIsInModule(this.userName,"VideoManagementModule").then(result => this.videoIndexerAuthenticated = result)
        this.userService.UserIsInModule(this.userName,"AuthorizationModule").then(result => this.userManagementAuthenticated = result)
        this.userService.UserIsInModule(this.userName,"WebChatModule").then(result => this.webAuthenticated = result)

      }
        }

      })
    
 
  }
 
  listUsers() {
    this.router.navigate(['users']);
  }
  logging() {
    this.router.navigate(['Feedback']);
  }
  listUserManagement() {
    this.router.navigate(['roles']);
  }

  listVideos() {
    this.router.navigate(['videos']);
  }

  showWebChat() {
    this.router.navigate(['webchat']);
  }

  languageModelVideos() {
    this.router.navigate(['videos_languagemodels']);
  } 
  
  authorisation() {
    this.router.navigate(['Nutzerverwaltung']);
  }  
  mediaOverview() {
    this.router.navigate(['Medien']);
  }  
 
  public get authenticated(): boolean {
    return this.adalService.userInfo.authenticated;
  }

  public get webauthenticated():boolean{
    return this.webAuthenticated
  }
  public get usermanagementAuthenticated():boolean{
    return this.userManagementAuthenticated
  }
  public get videoindexerAuthenticated():boolean{
    return this.videoIndexerAuthenticated
  }

}
