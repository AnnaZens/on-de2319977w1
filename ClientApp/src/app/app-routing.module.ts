import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AdalGuard } from 'adal-angular4';
import {RoleAssignmentComponent} from './role-assignment/role-assignment.component'
import { VideoLanguageModelComponent } from './video-language-model/video-language-model.component' ;


import {
  VideoListComponent,
  VideoListResolver
} from './videos/index'

import {
  VideoWidgetComponent,
  VideoWidgetRouteActivator
} from './video-widget/index'


import {
  UsersListComponent,
  UsersListResolver
} from './users/index'

import {WebChatComponent} from './web-chat/web-chat.component'
import { AuthorizationComponent } from './authorization/authorization.component';
import { MediaDashboardComponent } from './media-dashboard/media-dashboard.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { ModuleAssignmentComponent } from './module-assignment/module-assignment.component';
import { LoggingComponent } from './logging/logging.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AdalGuard] },
  { path: 'Medien/videos/:id', component: VideoWidgetComponent },
  { path: 'Medien/videos', component: VideoListComponent, resolve: {videos:VideoListResolver} },
  { path: 'Medien/sprachmodelle', component: VideoLanguageModelComponent },
  { path: 'users', component: UsersListComponent, resolve: {videos:UsersListResolver} },
  { path: 'Nutzerverwaltung/Rollen', component: RoleAssignmentComponent, resolve: {videos:UsersListResolver} },
  { path: 'webchat', component: WebChatComponent },
  { path: 'Nutzerverwaltung', component: AuthorizationComponent },
  { path: 'Medien', component: MediaDashboardComponent },
  { path: 'Feedback', component: LoggingComponent },
  { path: 'Nutzerverwaltung/Module', component: ModuleAssignmentComponent },
  { path: 'Nutzerverwaltung/Benutzer', component: UserManagementComponent },
  { path: '', component: DashboardComponent },
  { path: '**', component: NotFoundComponent },
  { path: '404', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
