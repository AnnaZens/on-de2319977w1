import {Component, OnInit, Output, Input} from '@angular/core';
import {VideoIndexerService } from '../videos/videoindexer.service'
import {ActivatedRoute} from '@angular/router';
import {VideoWidget} from '../video-widget/video-widget'
import { DomSanitizer } from "@angular/platform-browser";
import { AdalService } from 'adal-angular4';
import { ToastrService } from 'ngx-toastr';

import {
  VideosApi, 
  DtoSearchVideosResponseItem, 
  DtoListVideosResponse,
  DtoSearchVideosResponse,
  DtoListVideosResponseItem,
  DtoSearchVideosRequest,
  DtoSearchVideosRequestInsightsEnum,
  DtoLanguageResponse,
} from '../services/videoindexer/api'

@Component({
  selector: 'app-video-widget',
  templateUrl: './video-widget.component.html',
  styleUrls: ['./video-widget.component.css']
})

export class VideoWidgetComponent implements OnInit {
  // videoWidget: VideoWidget;
  widgetplayer: string;
  widgetinsights: string;
  id: string;

  constructor(private adalService: AdalService, private toastr: ToastrService, private viService: VideoIndexerService, private router: ActivatedRoute, private sanitizer: DomSanitizer) {

  }

  ngOnInit() {
    this.id = this.router.snapshot.params['id'];
    // this.viService.getVideoWidget(this.id).subscribe(details => 
    //   {
    //     this.widgetinsights = details.widgetinsights;
    //     this.widgetplayer = details.widgetplayer;
    //   })    
    this.toastr.info("Das Video wird geladen...")

    this.viService.getVideoWidgetAsync(this.id).then(details => 
        {
          this.widgetinsights = details.widgetinsights;
          this.widgetplayer = details.widgetplayer;
        })  
  }


  getSantizeUrl(url : string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  public get authenticated(): boolean {
    return this.adalService.userInfo.authenticated;
  }
}
