import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {VideoIndexerService} from '../videos/videoindexer.service'
import { ToastrService } from 'ngx-toastr';


@Injectable()
export class VideoWidgetRouteActivator implements CanActivate{
  constructor(private viService: VideoIndexerService, private toastr: ToastrService, private router: Router){

  }

  canActivate(route: ActivatedRouteSnapshot){
    const viExists = !!this.viService.getVideoWidget(route.params['id'])

    if (!viExists) {
      this.router.navigate(['/404']);
    }
    return viExists
  }
}
