import { Component, OnInit, OnDestroy, ViewChild, ViewContainerRef, ComponentRef, Type, ComponentFactory, ComponentFactoryResolver } from '@angular/core';
import { AdalService } from 'adal-angular4';
import {Router, NavigationEnd, ActivatedRouteSnapshot } from '@angular/router'
import { ToastrService } from 'ngx-toastr';
import {Subject, Observable, Subscription} from 'rxjs/RX';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit, OnDestroy  {
  
  @ViewChild("toolbarTarget", {read: ViewContainerRef})
  toolbarTarget: ViewContainerRef;

  toolbarComponents: ComponentRef<Component>[] = new Array<ComponentRef<Component>>();
  routerEventSubscription: Subscription;
  links: Array<{ text: string, path: string }> = new Array<{ text: string, path: string }>();
  UserName: string 

  constructor(private adalService: AdalService, private toastr: ToastrService, private router: Router,  private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {

    this.adalService.handleWindowCallback();
    this.adalService.getUser().subscribe(user => this.UserName = user.userName)
    
    this.links.push({ text: 'Dashboard', path: '' });
        
    this.routerEventSubscription = this.router.events.subscribe(
      (event) => {      
          if (event instanceof NavigationEnd) {
            console.log(event.url)
            this.fillLinksOnChange(event.url)
          }
      });
  }

  fillLinksOnChange(url:string):void{
    this.links = new Array<{ text: string, path: string }>();
    var urlArray = url.split("/")
    this.links.push({ text: 'Dashboard', path: '' });
    var path = ""
    
    urlArray.forEach((element,index) => {
      if(index !== 0 && element.length>0) {
      
        if(index == urlArray.length-1){
          path = path+element
        }
        else{path = path+element+"/"
        }
      this.links.push({ text: element.charAt(0).toUpperCase() + element.slice(1), path: path });
      console.log(path)
      }


    })

  }

  ngOnDestroy(): void {
    //this.routerEventSubscription.unsubscribe();
  }

  login() {
    this.adalService.login();
  }

  logout() {
    this.adalService.logOut();
  }

  listUsers() {
    this.router.navigate(['users']);
  }

  listUserManagement() {
    this.router.navigate(['roles']);
  }

  listVideos() {
    this.router.navigate(['videos']);
  }

  languageModelVideos() {
    this.router.navigate(['videos_languagemodels']);
  }  

  showWebChat() {
    this.router.navigate(['webchat']);
  }
  showDashboard(){
    this.router.navigate(['']);

  }
  public get authenticated(): boolean {
    return this.adalService.userInfo.authenticated;
  }
}
