// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config: {
    /* tenant: 'corpdir.onmicrosoft.com',
    clientId: '123f75e3-5932-434b-99a1-ae998b79aa25', */
    tenant: 'marcusjaintadeey.onmicrosoft.com',
    clientId: 'b22667d2-0471-4226-8c24-14853e05b6cf', 
    cacheLocation: 'localStorage',
    endpoints: {
      'https://graph.microsoft.com': '00000003-0000-0000-c000-000000000000'
    }
  },


  VIDEOINDEXER_API_BASE_PATH: 'https://dianaframeworkvideoindexerapi.azurewebsites.net',
    ADMINPORTAL_API_BASE_PATH: 'https://dianaframeworkadminportalapi.azurewebsites.net',
    LOGANALYTICS_API_BASE_PATH: 'https://dianaframeworkloganalyticsapi.azurewebsites.net' 
};

/*03a7bcb4-7f64-408b-8555-55f76dd89fa6
* b22667d2-0471-4226-8c24-14853e05b6cf
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
